type Profile::Pf::Rdr = Struct[{
  ip                 => Variant[Stdlib::IP::Address, String[1]],
  external           => Stdlib::Port,
  Optional[internal] => Stdlib::Port,
  Optional[proto]    => Enum[tcp, udp],
}]
