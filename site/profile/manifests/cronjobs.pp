# a profile for configuring cron jobs
class profiles::cronjobs {
  $cronjob_defaults = lookup('profile::cronjobs_defaults', Hash, undef, {})
  $cronjobs = lookup('profile::cronjobs', Hash, undef, {})

  $cronjobs.each |$v, $d| {
    cron { $v:
      * => deep_merge($cronjobs_defaults, $d),
    }
  }
}
