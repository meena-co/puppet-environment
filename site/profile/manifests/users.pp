# manage users with puppet accounts module
class profile::users (
  Hash $defaults  = {},
  Hash $users     = {},
){
  $users.each |$u, $data| {
    accounts::user { $u:
      * => deep_merge($defaults, $data),
    }
  }
}
