class profile::devfs {

  $defaults = {
    ensure => present,
    path   => '/etc/devfs.rules',
    notify => Service[devfs]
  }

  file_line { 'jail_bpf':
    line => '[jail_bpf=5]',
    *    => $defaults,
  }
  file_line { 'jail_bpf_add_include':
    line  => 'add include $devfsrules_jail',
    after => '[jail_bpf=5]',
    *     => $defaults,
  }
  file_line { 'jail_bpf_add_bpf_unhide':
    line  => "add path 'bpf*' unhide",
    after => 'add include \$devfsrules_jail',
    *     => $defaults,
  }

  service { 'devfs':
    ensure     => running,
    hasstatus  => false,
    hasrestart => true,
  }
}
