# entry point for configuring pf
# for now it's mostly used for NATing jails
class profile::pf (
  Optional[Stdlib::IP::Address::V6]        $nat6_address = undef,
  Optional[Stdlib::IP::Address::V4]         $nat_address = undef,
  Optional[String]                        $nat_interface = undef,
  Optional[Hash[String, Profile::Pf::Rdr]] $nat_forwards = {},
){
  # nat_forwards => {identifier => {ip => IP, proto => String, external => port, internal => port}}

  $nat_enable = ($nat_address and $nat_interface)
  if !$nat_forwards.empty and !$nat_enable {
    fail("Please enable NAT by setting 'nat_address' and 'nat_interface', " +
    "otherwise setting any 'nat_forwards' makes no sense!")
  }

  $pf_conf_base = 'scrub in all'

  if $nat_enable {
    $pf_nat_base  = "nat pass on ${nat_interface} from ${nat_address} to any -> (${nat_interface}:0)"
  } else {
    $pf_nat_base  = undef
  }

  if !$nat_forwards.empty {
    $pf_nat_forw  = $nat_forwards.map |$id, $rdr| {
      # from https://www.openbsd.org/faq/pf/rdr.html
      # pass in on egress proto $proto from any to any port $external_port rdr-to $ip [$internal_port]
      $ip = $rdr[ip]
      $_proto = $rdr[proto]? {
        undef   => 'tcp',
        default => $rdr[proto],
      }
      $_port = $rdr[internal] ? {
        undef => $rdr[external],
        default => $rdr[internal],
      }

      $v4_rules = "rdr on ${nat_interface} proto ${_proto} from any to ${nat_interface} port ${rdr[external]} -> ${ip} port ${_port}"

      $v6_rules = @("V6"/L)
      nat on ${nat_interface} inet6 from ${ip} to any -> ${nat6_address} static-port
      rdr on ${nat_interface} inet6 proto ${_proto} from any to ${nat6_address} port ${rdr[external]} -> ${ip} port ${_port}
      |V6
      $rules = $ip? {
        Stdlib::IP::Address::V4 => $v4_rules,
        Stdlib::IP::Address::V6 => $v6_rules,
        String                  => $v4_rules,
        default                 => fail("Not a valid IP address: '${ip}'")
      }
      $rules
    }
  } else {
    $pf_nat_forw = undef
  }

  # adding \n to the end of pf.conf(5) -- not sure this is documented
  $pf_conf = [$pf_conf_base, $pf_nat_base, $pf_nat_forw, "\n"].flatten.join("\n")

  file {'/etc/pf.conf':
    ensure  => file,
    content => $pf_conf,
    mode    => '0644',
    owner   => root,
    group   => wheel,
    notify  => Service[pf],
  }

  service { 'pf':
    ensure => running,
    enable => true,
  }
}
