# install thelounge globally
class profile::thelounge::install {

  $home  = $profile::thelounge::home
  $user  = $profile::thelounge::user
  $group = $profile::thelounge::group

  package { 'thelounge':
    ensure   => latest,
    provider => 'npm',
  }
}
