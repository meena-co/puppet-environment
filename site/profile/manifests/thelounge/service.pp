# create service file and control daemon
class profile::thelounge::service {

  $servicename  = $profile::thelounge::servicename
  $home  = $profile::thelounge::home
  $user  = $profile::thelounge::user
  $group = $profile::thelounge::group

  $daemon = {
    name    => $servicename,
    user    => $user,
    group   => $group,
    cwd     => $home,
    command => "/usr/bin/env THELOUNGE_HOME=${home} /usr/local/bin/thelounge start",
  }
  file { "/usr/local/etc/rc.d/${servicename}":
    ensure  => file,
    owner   => root,
    group   => wheel,
    mode    => '0755',
    content => epp('profile/rc.daemon.epp', $daemon),
    notify  => Service[thelounge],
  }

  service { $servicename:
    ensure => running,
    enable => true,
  }
}
