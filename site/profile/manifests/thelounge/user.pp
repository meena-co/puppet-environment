# create user and its home dir
class profile::thelounge::user {

  $home  = $profile::thelounge::home
  $user  = $profile::thelounge::user
  $group = $profile::thelounge::user

  group { $group:
    ensure => 'present',
    system => true,
  }
  user { $user:
    ensure => 'present',
    gid    => $group,
    system => true,
    home   => $home,
  }

  file { $home:
    ensure => directory,
    owner  => $user,
    group  => $group,
  }
}
