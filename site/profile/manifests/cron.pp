# Wrapper profile for voxpupuli/cron
class profile::cron {
  # this allows configuration via Hiera
  class { 'cron':
    manage_package => false,
  }
}