# installation & basic setup of *our* Apache httpd
class profile::apache (
  Hash $mdvhost_defaults = {},
  Hash $mdvhosts         = {},
){

  class { 'apache':
    mpm_module        => 'event',
    error_log         => 'syslog:daemon:',
    serveradmin       => 'me@eena.me',
    default_vhost     => false,
    default_ssl_vhost => false,
    timeout           => 300,
    log_formats       => { vhost_combined => '%V %a %l %u %t \"%r\" %>s %b \"%{Referer}i\" \"%{User-Agent}i\"' },
    default_mods      => [
      alias,
      authn_core,
      deflate,
      dir,
      expires,
      headers,
      md,
      mime,
      mime_magic,
      proxy,
      proxy_http,
      remoteip,
      reqtimeout,
      socache_shmcb,
      watchdog,
      unique_id,
      ssl,
      http2,
      setenvif,
      status,
    ],
  }

  $crs_path = '/usr/local/etc/modsecurity/owasp-modsecurity-crs'
  apache::mod { 'security2':
    package => 'ap24-mod_security',
    before  => Vcsrepo[$crs_path],
  }
  vcsrepo { $crs_path:
    ensure   => latest,
    provider => 'git',
    source   => 'https://github.com/coreruleset/coreruleset.git',
    revision => 'v3.3.2',
  }

  apache::listen {['80', '443']: }

  apache::custom_config { 'custom-log':
    content => "CustomLog \"|/usr/bin/logger -t apache24_access -p daemon.info\" vhost_combined\n",
  }
  apache::custom_config { 'protocols':
    content => "Protocols h2 http/1.1\n",
  }

  $crs = [
    "Include ${crs_path}/crs-setup.conf.example",
    "Include ${crs_path}/rules/*.conf",
  ]
  apache::custom_config { 'configure crs':
    priority => 15,
    content  => "${crs.join("\n")}\n",
  }

  $md = [
    'MDBaseServer on',
    'MDCertificateAgreement https://letsencrypt.org/documents/LE-SA-v1.2-November-15-2017.pdf',
    'MDRequireHttps permanent', # redirects http to https
    'ServerAdmin me@eena.me', # set contact information
  ]
  apache::custom_config { 'configure md':
    priority => 35,
    content  => "${md.join("\n")}\n",
  }

  $mdvhosts.each |$v, $d| {
    apache::vhost::custom { $v:
      priority => pick($d['priority'], '25'),
      content  => epp('profile/mdvhost.epp',
                      deep_merge($mdvhost_defaults,
                                 delete($d, "priority"),
                                 { server_name => $v })),
    }
  }
}
