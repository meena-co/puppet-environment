# install & configure the quassel webserver
class profile::thelounge (
  String               $servicename = 'thelounge',
  String               $user        = 'thelounge',
  String               $group       = 'thelounge',
  Stdlib::Absolutepath $home        = "/srv/${servicename}",
){
  require profile::nodejs

  contain profile::thelounge::user
  contain profile::thelounge::install
  contain profile::thelounge::configure
  contain profile::thelounge::service

  Class[profile::thelounge::user]
  -> Class[profile::thelounge::install]
  ~> Class[profile::thelounge::configure]
  ~> Class[profile::thelounge::service]

  Class[profile::thelounge::install]
  ~> Class[profile::thelounge::service]
}
