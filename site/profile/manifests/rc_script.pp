class profile::rc_script {
  $rc_script_defaults = lookup('profile::rc_script_defaults', Hash, undef, {})
  $rc_scripts = lookup('profile::rc_scripts', Hash, undef, {})

  $rc_scripts.each |$v, $d| {
    rc_script { $v:
      * => deep_merge($rc_script_defaults, $d),
    }
  }
}
