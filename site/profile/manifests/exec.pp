# exec
class profile::exec {
  $exec_defaults = lookup('profile::exec_defaults', Hash, undef, {})
  $execs = lookup('profile::execs', Hash, undef, {})

  $execs.each |$v, $d| {
    exec { $v:
      * => deep_merge($exec_defaults, $d),
    }
  }
}
