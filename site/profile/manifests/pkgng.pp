# Manage pkg.conf settings and repos
class profile::pkgng {
  contain pkgng

  $pkg_repo_defaults = lookup('profile::pkg_repo_defaults', Hash, undef, {})
  $pkg_repos = lookup('profile::pkg_repos', Hash, undef, {})

  $pkg_repos.each |$p, $d| {
    pkgng::repo { $p:
      * => deep_merge($pkg_repo_defaults, $d),
    }
  }
}
