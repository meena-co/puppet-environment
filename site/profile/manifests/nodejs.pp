# install nodejs and configure npm into usability
class profile::nodejs {

  class { 'nodejs':
    nodejs_package_name   => 'node14',
    nodejs_package_ensure => installed,
    npm_package_name      => 'npm-node14',
    npm_package_ensure    => installed,
    npm_path              => '/usr/local/bin/npm'
  }

  nodejs::npm::global_config_entry { 'unsafe-perm':
    ensure => 'present',
    value  => 'true',
  } ->
  nodejs::npm::global_config_entry { 'allow-root':
    ensure => 'present',
    value  => 'true',
  }
}
