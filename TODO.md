# Steps I've taken to setup a static site

please move them into README.md once implemented (automated).

## Kernel

for proper (network) isolation, we'll need to recompile the kernel (option VIMAGE) or wait for FREEBSD-12 :D

## template jails

I've created two template jails

- ats01
- node8
- php72
- elixir16

They contain each container puppet5 and git-lite packages, and then have trafficserver and node8, respectively.
These two templates can be used to spawn jails from and then install applications into.

## ATS jail

This jail will need a VDEV device. i.e.: A `zfs volume` that's passed thru as device.
This way, trafficserver can get a device as raw disk.

## application jails

to be able to install applications, there might be a series of preconditions

- checkout app from git
- private git needs ssh auth
- ssh auth needs /root bind-mount into jail

Even tho this is a jail, we still want to work as unprivileged users!

## Application server

Just in case we need a server, our templates have an (inactive) httpd24 installed.

## using puppet

to make puppet universally usable, we (ro) bind-mount /usr/local/etc/puppet into the containers

## tarsnap backups

None of the above things need backing up, so tarsnap is not installed in any of these containers
